import json
import matplotlib.pyplot as plt
from analysejson import photoset


def stringtoint(g):
  '''
  Convertit un caractere en entier
  param g:  Chaine de caratere (<class list> nombre)
  
  Retourne le nombre correspondant
  '''
  l = []
  for e in g:
    l.append(int(e))
  return l


def grapheunjour(filename, datelist, j, m):
  """
  Returns the figure on a day  \n 
  of the brightness based on time_of_day
  
    :param filename: the name of the file you want to analyze
    :param datelist: a list of tuples of the form (year, month, day)
    :param j: day of the month
    :param m: month

  """
  unjourbright = []
  unjourtimeofday = []
  jour = 0
  data = photoset(filename)
  photo = data.get_photo()
  for i in datelist:
    if i[1] == m and i[2] == j:
        unjourbright.append(data.get_brightness(jour))
        unjourtimeofday.append(data.get_brightness(jour))
    jour += 1

  # Liste de brightness de photo sur un jour
  unjourbright = []
  # Liste de time_of_day de photo sur un jour
  unjourtimeofday = []
  jour = 0
  data = photoset(filename)
  photo = data.get_photo()

  for i in datelist:
    if i[1] == m and i[2] == j:
        unjourbright.append(data.get_brightness(jour))
        unjourtimeofday.append(data.get_brightness(jour))
    jour += 1


def time_delta(data):
    '''
    Tri les photo d de data en fonctino du datetime\n
    Affiche le debut de la prise d'un pot_set \n
    Et la fin de cette dernière
    
    param data: Donnée à analyser (<class photoset>)
    '''
    ps_sorted = sorted(data.get_photo(), key=lambda d: d["datetime"])
    print(ps_sorted[0]["image_shooting"])
    print(ps_sorted[-1]["image_shooting"])


# Recuperer les données de filename
filename = "no_gps/68cc8d25.json"
data = photoset(filename)
photos = data.get_photo()

time_of_day = []
brightness = []
daylight = []
date = []
datetmp = []

# Recuperer les information des photos
for photo in photos:
  time_of_day.append(photo["time_of_day"])            # time_of_day
  brightness.append(photo["brightness"])              # brightness
  daylight.append(photo["attributes"]["daylight"])    # daylight
  #datetmp=photo["local_time"].split(" ")
  #date1 = stringtoint(datetmp[0].split(":"))
  #print(date1)
  #date2 = stringtoint(datetmp[1].split(":"))
  #date.append(date1)
  #date.append((photo["local_time"]).split(":",4))

# Duree d'un photo_set
time_delta(data)
# Figure
# affichage des données
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
fig = plt.figure(figsize=(10, 10))
# Trace de la brightness en fonction du time_of_day
ax1.plot(time_of_day, brightness, 'b.')
ax1.set_xlabel('time_of_day')
ax1.set_ylabel('brightness', color='b')
for tl in ax1.get_yticklabels():
    tl.set_color('b')

# Trace de la daylight en fonction du time_of_day
ax2.plot(time_of_day, daylight, 'r.')
ax2.set_ylabel('daylight', color='r')
ax2.set_xlabel('time_of_day', color='b')
for tl in ax2.get_yticklabels():
    tl.set_color('r')


plt.show()
