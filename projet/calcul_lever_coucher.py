from analysejson import usefuldata

# calcul la concentration des valeurs contenues dans values dans les intervals de taille 100/nb_inter
def concentration(values, nb_inter=10):
    '''
        Calcul la concentration des valeurs de time_of_day (comprises dans [0;1]) selon \n
        une partition de nb_inter intervals de [0;1]
        Parametres :
            values : liste de valeurs
            nb_inter : le nombre d'intervals à creer dans [0;1]. Si 100 n'est pas divisible par nb_inter --> retourne erreur et liste vide
        Sortie :
            densite_values : liste de liste, la ieme liste étant la liste des valeurs comprises dans l'interval [i/100, (i+100/nb_inter)/100]
    '''
    # retourne message d'erreur si nb_inter ne divise pas 100
    if 100 % nb_inter != 0:
        print("Erreur : La taille des sous-intervals demandée n'est pas un multiple de 100 !")
        return []

    # création des intervals
    intervals = []
    for i in range(0, 100, 100/nb_inter):
        intervals += [[i/100, (i+100/nb_inter)/100]]

    densite = [0 for i in range(0,nb_inter)]
    densite_values = [[] for i in range(0,nb_inter)]
    for v in values:
        k = 0
        for i in intervals:
            if v >= i[0] and v < i[1]:
                densite_values[k].append(v)
            k += 1

    return densite_values
    
# prend en parametres une partition du set de photo (sous-set)
# def lever_coucher(photo_set):
def lever_coucher(data):
    """
    Renvoie le lever et le coucher du soleil 
    de l'ensemble des photo obtenu grace à daylight et time_of_day
    
    :param data(class photoset):Donnée a analyser 
    
    Return:
        (tuple) tod_lever, tod_coucher: sous format time_of_day l'heurede lever et de coucher du soleil
    """
    # avec daylight
    time_of_day = data.get_time_of_day() 
    daylight = data.get_daylight()
    # valeur prises en dessous de 0.1 de daylight pour calculer debut et fin du jour
    epsilon = 0.1
    time_low_daylight=[]
    time_high_daylight=[]
    there_is_daylight_low = False
    k = 0
    for d in daylight:
        if d < epsilon:
            time_low_daylight.append(time_of_day[k])
            there_is_daylight_low = True
        else:
            time_high_daylight.append(time_of_day[k])
        k += 1

    if there_is_daylight_low:
        densite_low_daylight = concentration(time_low_daylight)
        densite_high_daylight = concentration(time_high_daylight)

        # on determine si le jour est à cheval sur 1-0 ou si il est dans l'interval
        if len(densite_high_daylight[0]) > 0 and len(densite_high_daylight[-1]) > 0:
            # on est ici dans le cas ou le jour est a cheval sur 1-0 (gps/d642bf05.json)
            # dans ce cas, le début du jour est dans l'inteval le max de time_low_daylight
            tod_lever = max(time_low_daylight)
            # et la fin du jour est le min de time_low_daylight
            tod_coucher = min(time_low_daylight)
        else:
            # on est ici dans le cas ou le jour est centre dans 0-1 (no_gps/0b34ff9e.json)
            # determination de l'heure de lever
            indice_max = 0
            # on avance tant qu'il n'y a pas de valeurs low
            while len(densite_low_daylight[indice_max]) < 1:
                indice_max += 1
            # quand il y en a, on avance jusqu'à ce qu'il n'y en ai plus
            while len(densite_low_daylight[indice_max]) > 0:
                indice_max += 1
            indice_max -= 1
            tod_lever = max(densite_low_daylight[indice_max])

            # determination de l'heure de coucher, meme chose mais en partant du dernier interval
            indice_max = -1
            # on avance tant qu'il n'y a pas de valeurs low
            while len(densite_low_daylight[indice_max]) < 1:
                indice_max -= 1
            # quand il y en a, on avance jusqu'à ce qu'il n'y en ai plus
            while len(densite_low_daylight[indice_max]) > 0:
                indice_max -= 1
            indice_max += 1
            tod_coucher = max(densite_low_daylight[indice_max])

    else:
        # ici, il n'y a pas de données avec une daylight basse dans le set de données.
        # on prend donc les valeurs par rapport aux valeurs de début et de fin des valeurs hautes
        # on determine si le jour est à cheval sur 1-0 ou si il est dans l'interval
        if len(densite_high_daylight[0]) > 0 and len(densite_high_daylight[-1]) > 0:
            # on est ici dans le cas ou le jour est a cheval sur 1-0 (gps/d642bf05.json)
            # determination de l'heure de coucher
            indice_max = 0
            # on avance tant qu'il y a des valeurs high
            while len(densite_high_daylight[indice_max]) > 0:
                indice_max += 1
            indice_max -= 1
            tod_coucher = max(densite_high_daylight[indice_max])

            # determination de l'heure de lever, meme chose mais en partant du dernier interval
            indice_max = -1
            # on avance tant qu'il y a des valeurs high
            while len(densite_high_daylight[indice_max]) > 0:
                indice_max -= 1
            indice_max += 1
            tod_lever = max(densite_high_daylight[indice_max])
        else:
            # on est ici dans le cas ou le jour est centre dans 0-1 (no_gps/0b34ff9e.json)
            # dans ce cas, le début du jour est dans l'inteval le min de time_high_daylight
            tod_lever = min(time_high_daylight)
            # et la fin du jour est le max de time_high_daylight
            tod_coucher = max(time_high_daylight)

    return tod_lever, tod_coucher

def calculer_duree_du_jour(tod_lever, tod_coucher):
    # cas ou le lever est inferieur au coucher en time_of_day
    if tod_coucher < tod_lever:
        tod_coucher += 1
    return tod_coucher - tod_lever

    

photo_set = usefuldata('no_gps/0b34ff9e.json')

