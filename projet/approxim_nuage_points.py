from time import time
from numpy.polynomial.polynomial import polyfit
import numpy as np
import matplotlib.pyplot as plt
from analysejson import photoset


# modelise le sous-set
<<<<<<< HEAD
filename = 'no_gps/0c577c14.json'
data = photoset(filename)
photos = data.get_photo()
=======
photo_set = usefuldata('no_gps/1db50fea.json')
photos = photo_set["photo_set"]["photos"]
>>>>>>> 3667d905080590e651720d95ff0384f827b7c3e4
photos_to_app = photos[0:2000]

time_of_day = data.get_time_of_day() 
brightness = data.get_brightness()
daylight = data.get_daylight()

# approximation second degre methode des moindres carrés du nuage de points (time_of_day, brightness)
pol = polyfit(time_of_day, brightness, 2)
print(pol)
# polynome
indice = np.arange (0,101,1)
x = indice/100
y = pol[0] + pol[1]*x + pol[2]*x*x

# affichage des données
fig, ax1 = plt.subplots()
ax2 = ax1.twinx()
fig  = plt.figure(figsize=(10,10))
# Trace de la brightness en fonction du time_of_day
ax1.plot(time_of_day, brightness, 'b.')
ax1.set_xlabel('time_of_day')
ax1.set_ylabel('brightness', color='b')
for tl in ax1.get_yticklabels():
    tl.set_color('b')
ax1.plot(x, y)

# Trace de la daylight en fonction du time_of_day
ax2.plot(time_of_day, daylight, 'r.')
ax2.set_ylabel('daylight', color='r')
ax2.set_xlabel('time_of_day', color='b')
for tl in ax2.get_yticklabels():
    tl.set_color('r')


plt.show()
