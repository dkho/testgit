import numpy as np
import matplotlib.pyplot as plt
from geopy.distance import geodesic
from scipy.interpolate import interp1d


def latitude(duree_du_jour, jour):
    """Calcule la lattitude su photo_set

    Args:
        duree_du_jour (sous format datetime): temps entre le lever et le coucher du soleil
        jour (sous format datetime):jour a analyser 

    Returns:
        entier: latitude de la localistion du photoset
    """
    H = (duree_du_jour/48)*360  # degree
    tphy = -np.cos(H/np.tan(declinaison_soleil(jour)))
    return np.atan(tphy)


def declinaison_soleil(jour):
    '''
    param jour: jour sous format datetime

    '''
    return (180/np.pi)*(np.sin(np.pi*23.45/180)*np.sin(2*np.pi/365.25)*(jour-81))


def longitude(ms_photoset, ms_utc):
    '''
    param ms_photoset: midi solaire du photoset (sous format datetime)
    param ms_utc: midi solaire utc (sous format dattime)
    
    Retourne la longitude (donnée de la localisation du photoset)
    '''
    resa = ms_utc - ms_photoset
    res = abs(resa)
    res = res*86400
    h = res//3600
    m = (res - 3600*h)//60
    s = res - 3600*h - 60*m
    d = h*15+(15*m)/60+(15*s)/3600
    if resa > 0:
        d = d*(-1)
    return d

#print(longitude(0.5474074074,0.75050925925 ))


def Score(Place1, Place2):
    '''
    param Place1: tuple de longitude et latitude de la premiere localisation
    param Place2: tuple de la latitude et latitude de la seconde localisation
    
    Retourne un scrore sur [0,1] de la distance entre ces deux localisations \n
    
    '''
    distance = geodesic(Place1, Place2).miles
    nbt = 100000
    xi = np.linspace(0, 2000, nbt)
    yi = np.linspace(0, 1, nbt)[::-1]

    # Estimation du score
    for i in range(1, nbt):
        if distance >= xi[i-1] and distance <= xi[i]:
            return yi[i]
    if distance > xi[i]:
        return 0
